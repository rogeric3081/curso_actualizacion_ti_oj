import { Module} from '@nestjs/common'
import { ParametricasModule } from './parametricas/parametricas.module'
import { ProductoModule } from './productos/producto.module'
import { PedidosModule } from './pedidos/pedido.module'

@Module({
  imports: [ParametricasModule, ProductoModule, PedidosModule],
})
export class ApplicationModule {}
