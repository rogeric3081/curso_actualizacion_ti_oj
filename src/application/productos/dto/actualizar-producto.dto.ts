import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class UpdateProductoRequestDTO {
  @ApiProperty({
    example: 'P-001',
  })
  @IsNotEmpty({
    message: 'El código del producto no puede ser vacío',
  })
  codigo: string

  @ApiProperty({
    example: 'Producto X',
  })
  @IsNotEmpty({
    message: 'El nombre del producto no puede ser vacío',
  })
  nombre: string
  @ApiProperty({
    example: '10.99',
  })
  @IsNotEmpty({
    message: 'El precio del producto no puede ser vacío',
  })
  precio: number
}
