import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CrearProductoRequestDTO {
  @ApiProperty({ example: 'A-001' })
  @IsNotEmpty({ message: 'El código no puede ser vacío' })
  codigo: string

  @ApiProperty({ example: 'Producto X' })
  @IsNotEmpty({ message: 'El nombre del producto no puede ser vacío' })
  nombre: string

  @ApiProperty({ example: '10.99' })
  @IsNotEmpty({ message: 'El precio del producto no puede ser vacío' })
  precio: number
}

export class CrearProductoResponseDTO {
  @ApiProperty({
    example: '1',
    description: 'Número de indica el identificador del producto',
  })
  @IsNotEmpty({
    message: 'Id para el producto no puede ser vacío',
  })
  id: string
}
