import { BaseController } from 'src/common/base'
import { ProductoService as ProductoService } from '../service/producto.services'
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Put,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { CrearProductoRequestDTO } from '../dto/crear-producto.dto'
import { ParamIdDto } from 'src/common/dto/params-id.dto'
import { UpdateProductoRequestDTO } from '../dto/actualizar-producto.dto'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'
import { JwtAuthGuard } from 'src/core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from 'src/core/authorization/guards/casbin.guard'
import { ApiOperation, ApiQuery } from '@nestjs/swagger'

@UseGuards(JwtAuthGuard, CasbinGuard)
@Controller('productos')
export class ProductoController extends BaseController {
  constructor(private service: ProductoService) {
    super()
  }
  @ApiOperation({
    summary:
      'Obtiene productos con posibilidad filtrar por categoria y nombre ',
  })
  @ApiQuery({ name: 'categoria', type: String, required: false })
  @ApiQuery({ name: 'nombre', type: String, required: false })
  @Get()
  async listar(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Query('categoria') categoria?: string,
    @Query('nombre') nombre?: string
  ) {
    const result = await this.service.listar(
      paginacionQueryDto,
      categoria,
      nombre
    )
    return this.successListRows(result)
  }

  @Post()
  async crear(@Req() req: Request, @Body() dto: CrearProductoRequestDTO) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.service.crear(dto, usuarioAuditoria)
    return this.successCreate(result)
  }
  @Get()
  async all(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Req() req: Request
  ) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.service.all(paginacionQueryDto, usuarioAuditoria)
    return this.successListRows(result)
  }
  @Get(':id')
  async getForId(@Param() params: ParamIdDto) {
    const { id: idProducto } = params
    const result = await this.service.getForId(idProducto)
    return this.success(result)
  }
  @Put(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() productoDto: UpdateProductoRequestDTO
  ) {
    const { id: idProducto } = params
    const usuarioAuditoria = '1'
    const result = await this.service.actualizarDatos(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id')
  async update(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() dto: UpdateProductoRequestDTO
  ) {
    const { id: idProducto } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.service.update(idProducto, dto, usuarioAuditoria)
    return this.successUpdate(result)
  }
  @Delete(':id')
  public async deleteUser(@Param() params: ParamIdDto, @Req() req: Request) {
    const { id: idProducto } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.service.delete(idProducto, usuarioAuditoria)
    return this.successDelete(result)
  }
}
