import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Producto } from './entity'
import { ProductoService } from './service/producto.services'
import { ProductoRepository } from './repository'
import { ProductoController } from './controller'

@Module({
  imports: [TypeOrmModule.forFeature([Producto])],
  exports: [ProductoService],
  providers: [ProductoService, ProductoRepository],
  controllers: [ProductoController],
})
export class ProductoModule {}
