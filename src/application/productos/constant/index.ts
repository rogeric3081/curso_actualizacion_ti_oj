export enum ProductoEstado {
  ACTIVO = 'ACTIVO',
  INACTIVO = 'INACTIVO',
  ELIMINADO = 'ELIMINADO',
}
export enum DetallePedidoEstado {
  ACTIVO = 'ACTIVO',
  INACTIVO = 'INACTIVO',
}