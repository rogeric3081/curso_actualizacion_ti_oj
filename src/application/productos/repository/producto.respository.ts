import { Injectable } from '@nestjs/common'
import { DataSource } from 'typeorm'
import { Producto } from '../entity'
import { CrearProductoRequestDTO } from '../dto/crear-producto.dto'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import { UpdateProductoRequestDTO } from '../dto/actualizar-producto.dto'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async delete(idProducto: string) {
    return await this.dataSource.getRepository(Producto).delete(idProducto)
  }

  async all(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)
    return await query.getManyAndCount()
  }
  async listar(
    paginacionQueryDto: PaginacionQueryDto,
    categoria?: string,
    nombre?: string
  ) {
    const { limite, saltar } = paginacionQueryDto
    let query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.estado',
        'producto.categoria',
      ])

    if (categoria) {
      query = query.where('producto.categoria = :categoria', { categoria })
    }

    if (nombre) {
      query = query.andWhere('producto.nombre like :nombre', {
        nombre: `%${nombre}%`,
      })
    }

    query = query.take(limite).skip(saltar)

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async update(
    id: string,
    dto: UpdateProductoRequestDTO,
    usuarioAuditoria: string
  ) {
    const { codigo, nombre, precio } = dto
    const datosActualizar: QueryDeepPartialEntity<Producto> = new Producto({
      codigo: codigo,
      nombre: nombre,
      precio: precio,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }
  async crear(datos: CrearProductoRequestDTO, usuarioAuditoria: string) {
    const { codigo, nombre, precio } = datos
    const producto = new Producto()
    producto.codigo = codigo
    producto.nombre = nombre
    producto.precio = precio
    producto.usuarioCreacion = usuarioAuditoria
    producto.usuarioModificacion = usuarioAuditoria
    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async getForId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }
}
