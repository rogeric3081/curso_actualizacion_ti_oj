import { BaseService } from 'src/common/base'
import { CrearProductoRequestDTO } from '../dto/crear-producto.dto'
import { ProductoRepository } from '../repository'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { UpdateProductoRequestDTO } from '../dto'
import { Messages } from 'src/common/constants/response-messages'

@Injectable()
export class ProductoService extends BaseService {
  constructor(
    @Inject(ProductoRepository)
    private repositorio: ProductoRepository
  ) {
    super()
  }
  async delete(idProducto: string, usuarioAuditoria: string) {
    //FIXME: Registrar que el usuario ID esta borrando
    console.log('Producto Borrado por usuario ID: ', usuarioAuditoria)
    return await this.repositorio.delete(idProducto)
  }

  async all(paginacionQueryDto: PaginacionQueryDto, usuarioAuditoria: string) {
    console.log('Usuario for Auditoria: ', usuarioAuditoria)
    return await this.repositorio.all(paginacionQueryDto)
  }

  async crear(datos: CrearProductoRequestDTO, usuarioAuditoria: string) {
    const result = await this.repositorio.crear(datos, usuarioAuditoria)
    return result
  }

  async getForId(idProducto: string) {
    const result = await this.repositorio.getForId(idProducto)
    if (!result) {
      throw new NotFoundException(
        `Producto con id ${idProducto} no fue encontrado`
      )
    }
    return result
  }

  async update(
    idProducto: string,
    datos: CrearProductoRequestDTO,
    usuarioAuditoria: string
  ) {
    const result = await this.repositorio.update(
      idProducto,
      datos,
      usuarioAuditoria
    )
    return result
  }
  async listar(
    paginacionQueryDto: PaginacionQueryDto,
    categoria?: string,
    nombre?: string
  ) {
    return await this.repositorio.listar(paginacionQueryDto, categoria, nombre)
  }

  async actualizarDatos(
    id: string,
    productoDto: UpdateProductoRequestDTO,
    usuarioAuditoria: string
  ) {
    const producto = await this.repositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    await this.repositorio.update(id, productoDto, usuarioAuditoria)
    return { id }
  }

  async buscarPorId(id: string) {
    const producto = await this.repositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    return producto
  }

  async eliminar(id: string, usuarioAuditoria: string) {
    const producto = await this.repositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }

    const dto = new UpdateProductoRequestDTO()

    const resultado = await this.repositorio.update(id, dto, usuarioAuditoria)
    return resultado
  }
}
