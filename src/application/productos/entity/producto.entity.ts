import { AuditoriaEntity } from 'src/common/entity/auditoria.entity'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { ProductoEstado } from '../constant'
import { UtilService } from 'src/common/lib/util.service'
import dotenv from 'dotenv'

dotenv.config()

@Check(UtilService.buildStatusCheck(ProductoEstado))
@Entity({ name: 'productos', schema: process.env.DB_SCHEMA_PRODUCTOS })
export class Producto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
    comment: 'Clave primaria de la tabla Producto',
  })
  id: string
  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Código de producto',
  })
  codigo: string
  @Column({
    type: 'varchar',
    length: 50,
    comment: 'Nombre de producto',
  })
  nombre: string
  @Column({
    type: 'float',
    comment: 'Precio unitario',
  })
  precio: number

  constructor(data?: Partial<Producto>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ProductoEstado.ACTIVO
  }
}
