import { Module } from '@nestjs/common'
import { PedidosController } from './controller'
import { PedidoService } from './service'
import { ProductoModule } from '../productos/producto.module'
import { PedidoRepository } from './repository'

@Module({
  controllers: [PedidosController],
  providers: [PedidoService, PedidoRepository],
  imports: [ProductoModule],
})
export class PedidosModule {}
