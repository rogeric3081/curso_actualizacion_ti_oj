import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Pedido } from '../entity'

import dayjs from 'dayjs'
import { CrearPedidoDto, PedidoFiltro } from '../controller/dto'

@Injectable()
export class PedidoRepository {
  constructor(private dataSource: DataSource) {}

  async listar(filtroPedidos: PedidoFiltro) {
    const { limite, saltar, fecha } = filtroPedidos
    const query = this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select([
        'pedido.id',
        'pedido.nroPedido',
        'pedido.fechaPedido',
      ])
      .take(limite)
      .skip(saltar)

    if (fecha) {
      query.where('pedido.fechaPedido = :fecha', { fecha })
    }

    return await query.getManyAndCount()
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    const datosGuardar: any = new Pedido({
      nroPedido: datosPedido.nroPedido,
      fechaPedido: dayjs(datosPedido.fechaPedido, 'YYYY-MM-DD').toDate(),
      usuarioCreacion: usuarioAuditoria,
    })
    const result = await this.dataSource
      .getRepository(Pedido)
      .save(datosGuardar)

    return result
  }
}