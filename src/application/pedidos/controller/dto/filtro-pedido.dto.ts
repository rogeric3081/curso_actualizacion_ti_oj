import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

export class PedidoFiltro extends PaginacionQueryDto {
  fecha: string
}