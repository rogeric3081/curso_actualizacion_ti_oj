# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.0.1 (2023-10-18)


### Features

* :sparkles: commit inicial ([f5068bd](https://gitlab.softwarelibre.gob.bo/capacitacion/nestjs-base-backend/commit/f5068bd0247569c547d7d6e1ce252c9804d33fa4))
